au! BufRead,BufNewFile *.txt call ShiftReportType()
function! ShiftReportType()
    if search('\c^\(DAST\s*Shift\s*report\).*$', 'n') > 0
        set ft=shiftreport
        setlocal comments=:-- commentstring=--\ %s
    endif
endfunction

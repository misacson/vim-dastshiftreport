if exists("b:current_syntax")
    finish
endif

syn match srTopic "\c^\(==\s\+\).*$"
syn match srOpen "\c^\(==\s\+\)\?Open issues.*$"
syn match srClosed "\c^\(==\s\+\)\?Closed issues.*$"
syn match srDiscuss "\c^\(==\s\+\)\?To discuss.*$"
syn match srFollow "\c^\(==\s\+\)\?To be followed up.*$"

syn match srClosedTag "\c\[Closed\]" contained

syn match srItem "^\*.*" contains=srClosedTag
syn match srComment "\s*#.*"
syn match srTitle "\c^\(DAST\s*Shift\s*report\).*$"
syn region srQuote start=/"/ skip=/\\"/ end=/"/


let b:current_syntax = "shiftreport"

hi def srTopic ctermfg=2 cterm=underline guifg=#b8bb26 gui=underline
hi def srOpen ctermfg=4 cterm=underline guifg=#83a598 gui=underline
hi def srClosed ctermfg=1 cterm=underline guifg=#fb4934 gui=underline
hi def srDiscuss ctermfg=34 cterm=underline guifg=#8ec07c gui=underline
hi def srFollow ctermfg=5 cterm=underline guifg=#d3869b gui=underline

hi def srClosedTag ctermfg=1 guifg=#fb4934

hi def srItem ctermfg=3 cterm=italic guifg=#fabd2f gui=italic
hi def link srComment Comment
hi def srTitle cterm=bold gui=bold
hi def srQuote ctermfg=9 cterm=italic guifg=#fe8019 gui=italic
